<?php
namespace DynamoDbORM\Tests;

use PHPUnit\Framework\TestCase;

use DynamoDbORM\DynamoDbModel;
use DynamoDbORM\DynamoDbClientService;

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;

class DynamoDbModelTest extends TestCase
{
    /**
     * @var App\Models\DynamoDbClientService
     */
    protected $dynamoDb;

    /**
     * @var App\Models\DynamoDbModel
     */
    protected $model;

    public function setUp()
    {
        $this->setUpDatabase();
    }

    private function setUpDatabase()
    {
        $config = [
            'credentials' => [
                'key' => 'dynamodb_local',
                'secret' => 'secret'
            ],
            'region' => 'test',
            'version' => 'latest',
            'endpoint' => 'http://localhost:8000'
        ];

        $dbc = new DynamoDbClient($config);
        $marshaler = new Marshaler($config);

        $this->dynamoDb = new DynamoDbClientService($dbc, $marshaler);
        $this->model = new MusicCollectionModel($this->dynamoDb);

        copy(__DIR__ . '/../dynamodb_local_init.db', __DIR__ . '/../dynamodb_local_test.db');
    }

    public function testCompositeKey()
    {
        $this->assertEquals(true, $this->model->hasCompositeKey());
    }

    public function testAliases()
    {
        $music = $this->model->create();
        $music->artist = 'My Artist';
        $this->assertEquals($music->artist, $music->Artist);
    }

    public function testFindItem()
    {
        $item = $this->model->find([
            'Artist' => 'Acme Band',
            'SongTitle' => 'Happy Day'
        ]);

        $this->assertEquals(4, count($item->getAttributes()));

        $this->assertEquals('Acme Band', $item->artist);
        $this->assertEquals('Happy Day', $item->songTitle);
        $this->assertEquals('Songs About Life', $item->albumTitle);
        $this->assertEquals(0, $item->listenCount);
    }

    public function testFindAllItems()
    {
        $items = $this->model->findAll();

        $this->assertEquals(3, count($items));
    }

    public function testCreateItem()
    {
        $music = $this->model->create();
        $music->artist = 'Madeon';
        $music->songTitle = 'You\'re On';
        $music->albumTitle = 'Adventure';
        $music->save();

        $query = [
            'TableName' => $this->model->getTable(),
            'Key' => [
                'Artist' => ['S' => $music->artist],
                'SongTitle' => ['S' => $music->songTitle],
            ]
        ];

        $result = $this->dynamoDb->getClient()->getItem($query);
        $item = $result['Item'];

        $this->assertEquals('Madeon', $item['Artist']['S']);
        $this->assertEquals('You\'re On', $item['SongTitle']['S']);
        $this->assertEquals('Adventure', $item['AlbumTitle']['S']);
    }

    public function testUpdateItem()
    {
        $newName = 'New Name';
        $key = [
            'Artist' => 'Acme Band',
            'SongTitle' => 'Happy Day'
        ];

        $music = $this->model->find($key);

        $music->albumTitle = $newName;
        $music->save();

        $query = [
            'TableName' => $this->model->getTable(),
            'Key' => [
                'Artist' => ['S' => $key['Artist']],
                'SongTitle' => ['S' => $key['SongTitle']],
            ]
        ];

        $result = $this->dynamoDb->getClient()->scan($query);
        $this->assertEquals(3, count($result['Items']));

        $query = [
            'TableName' => $this->model->getTable(),
            'Key' => [
                'Artist' => ['S' => $key['Artist']],
                'SongTitle' => ['S' => $key['SongTitle']],
            ]
        ];

        $result = $this->dynamoDb->getClient()->getItem($query);
        $this->assertEquals($newName, $result['Item']['AlbumTitle']['S']);
    }

    public function testDeleteItem()
    {
        $key = [
            'Artist' => 'Acme Band',
            'SongTitle' => 'Happy Day'
        ];

        $music = $this->model->find($key);
        $music->delete();

        $query = [
            'TableName' => $this->model->getTable(),
            'Key' => [
                'Artist' => ['S' => $key['Artist']],
                'SongTitle' => ['S' => $key['SongTitle']],
            ]
        ];

        $result = $this->dynamoDb->getClient()->getItem($query);

        $this->assertArrayNotHasKey('Item', $result);
    }

    public function testFindAllItemsWithEqualCondition()
    {
        for ($i = 0 ; $i < 10 ; $i++)
        {
            $query['RequestItems'][$this->model->getTable()][] = [
                'PutRequest' => [
                    'Item' => [
                        'Artist' => ['S' => 'Artist'.$i],
                        'SongTitle' => ['S' => 'SongTitle'.$i],
                        'AlbumTitle' => ['S' => 'My Album'],
                        'ListenCount' => ['N' => '10']
                    ]
                ]
            ];
        }

        $this->dynamoDb->getClient()->batchWriteItem($query);

        $items = $this->model->find()
                             ->where('ListenCount', 10)
                             ->get();

        $this->assertCount(10, $items);
    }

    public function testFindWithIndex()
    {
        $items = $this->model->find()
                             ->where('SongTitle', 'Happy Day')
                             ->get();

        $this->assertEquals('Acme Band', $items[0]->artist);
    }

    public function testFindFirstItem()
    {
        $item = $this->model->find()
                             ->where('SongTitle', 'Happy Day')
                             ->first();

        $this->assertTrue(is_object($item), "\$item must be an object. Current type : " . \gettype($item));
        $this->assertEquals('0', $item->listenCount);
    }
}

class MusicCollectionModel extends DynamoDbModel
{
    protected $tableName = 'MusicCollection';

    protected $hashKey = 'Artist';

    protected $rangeKey = 'SongTitle';

    protected $indexes = [
        'SongTitle' => 'ListenCountIndex'
    ];

    protected $aliases = [
        'artist' => 'Artist',
        'songTitle' => 'SongTitle',
        'albumTitle' => 'AlbumTitle',
        'listenCount' => 'ListenCount'
    ];
}
