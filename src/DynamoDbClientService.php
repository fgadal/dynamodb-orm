<?php
namespace DynamoDbORM;

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;

class DynamoDbClientService
{
    /**
     * @var \Aws\DynamoDb\DynamoDbClient
     */
    protected $dbc;

    /**
     * @var \Aws\DynamoDb\Marshaler
     */
    protected $marshaler;

    function __construct(DynamoDbClient $dbc, Marshaler $marshaler)
    {
        $this->dbc = $dbc;
        $this->marshaler = $marshaler;
    }

    public function getClient()
    {
        return $this->dbc;
    }

    public function getMarshaler()
    {
        return $this->marshaler;
    }
}
