<?php
namespace DynamoDbORM;

/**
 *
 */
class DynamoDbItem
{
    /**
     * @var \App\Models\DynamoDbModel
     */
    protected $model;

    /**
     * @var array
     */
    protected $attributes = [];

    public function __construct(DynamoDbModel $model, $attributes = [])
    {
        $this->model = $model;
        $this->hydrate($attributes);
    }

    public function save()
    {
        $this->model->save($this);
    }

    public function delete()
    {
        $this->model->delete($this);
    }

    public function getModel()
    {
        return $this->getModel;
    }

    public function getAttribute($key)
    {
        $key = $this->getAliasKey($key);
        if (array_key_exists($key, $this->attributes))
            return $this->attributes[$key];
    }

    public function setAttribute($key, $value)
    {
        $key = $this->getAliasKey($key);
        $this->attributes[$key] = $value;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    protected function hydrate($attributes)
    {
        foreach ($attributes as $key => $attr)
            $this->setAttribute($key, $attr);
    }

    protected function getAliasKey($key)
    {
        $aliases = $this->model->getAliases();
        if (array_key_exists($key, $aliases))
            return $aliases[$key];

        return $key;
    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }
}
