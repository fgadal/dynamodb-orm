<?php
namespace DynamoDbORM;

use Aws\DynamoDb\DynamoDbClient;

class DynamoDbQueryBuilder
{
    /**
     * @var \Aws\DynamoDb\DynamoDbClient
     */
    protected $dbc;

    /**
     * @var DynamodbModel
     */
    protected $model;

    /**
     * @var array
     */
    protected $where;

    public function __construct(DynamoDbModel $model)
    {
        $this->model = $model;
        $this->dbc = $model->getClient();
    }

    public function where($column, $operator = null, $value = null)
    {
        // We will make some assumptions about the operator. If only 2 values are
        // passed to the method, we will assume that the operator is an equals sign
        // and keep going. Otherwise, we'll require the operator to be passed in.
        if (func_num_args() == 2)
        {
            list($value, $operator) = [$operator, '='];
        }


        $this->where[$column] = [
            'value' => $value,
            'operator' => $operator
        ];

        return $this;
    }

     public function find($id)
     {
        foreach ($id as $column => $value)
        {
            $this->where($column, $value);
        }

        return $this;
    }

    public function findAll()
    {
        $query = [
            'TableName' => $this->model->getTable()
        ];

        $result = $this->dbc->scan($query)['Items'];

        $items = [];

        foreach ($result as $item)
        {
            $item = $this->model->unmarshalItem($item);
            $model = $this->model->newInstance($item);

            $items[] = $model;
        }

        return $items;
    }

    public function save(DynamoDbItem $item)
    {
        try
        {
            $this->dbc->putItem([
                'TableName' => $this->model->getTable(),
                'Item' => $this->model->marshalItem($item->getAttributes())
            ]);
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
        }
    }

    public function delete(DynamoDbItem $item)
    {
        try
        {
            $this->dbc->deleteItem([
                'TableName' => $this->model->getTable(),
                'Key' => $this->getDynamoDbKey($item)
            ]);
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
        }
    }

    public function first($columns = [])
    {
        return $this->get($columns, 1)[0];
    }

    public function get($columns = [], $limit = -1)
    {
        $query = [
            'TableName' => $this->model->getTable(),
            'ConsistentRead' => true
        ];

        $op = 'scan';

        if (!empty($colums))
        {
            $query['ProjectionExpression'] = implode(', ', $columns);
        }

        if ($limit > -1)
        {
            $query['Limit'] = $limit;
        }

        if (!empty($this->where))
        {
            if ($key = $this->conditionsContainKey())
            {
                $op = 'getItem';
                $query['Key'] = $this->model->marshalItem($key);
            }
            else if ($key = $this->conditionsContainIndexKey())
            {
                $op = 'query';
                $query['IndexName'] = $this->model->getIndexes()[$key];
                $query['KeyConditionExpression'] = $this->getDynamoDbExpression();
                $query["ExpressionAttributeNames"] = $this->getDynamoDbAttributeNames();
                $query['ConsistentRead'] = false;
                //$query['KeyConditionExpression'] = $column . $attrs['operator'] . ':v_' . strtolower($column);
            }

            if ($op == 'scan')
            {
                $query['FilterExpression'] = $this->getDynamoDbExpression();
                $query["ExpressionAttributeNames"] = $this->getDynamoDbAttributeNames();
            }

            $query['ExpressionAttributeValues'] = $this->getDynamoDbAttributeValues();
        }

        $result = $this->model->getClient()->{$op}($query);

        if (isset($result['Item']))
        {
            $item = $this->model->unmarshalItem($result['Item']);
            return new DynamoDbItem($this->model, $item);
        }
        else if (isset($result['Items']))
        {
            $items = [];

            foreach($result['Items'] as $item)
            {
                $item = $this->model->unmarshalItem($item);
                $item = new DynamoDbItem($this->model, $item);
                $items[] = $item;
            }

            return $items;
        }

        return;
    }

    public function getOperationType()
    {
        return $this->operationType;
    }

    protected function getDynamoDbKey(DynamoDbItem $item)
    {
        $keys = [];

        foreach ($this->model->getCompositeKey() as $key) {
            $k = $this->getSpecificDynamoDbKey($key, $item->getAttribute($key));

            if (is_array($k))
            {
                $keys = array_merge($keys, $k);
            }
        }

        return $keys;
    }

    protected function getSpecificDynamoDbKey($keyName, $value)
    {
        $idKey = $this->model->marshalItem([
            $keyName => $value
        ]);


        $key = [
            $keyName => $idKey[$keyName]
        ];

        return $key;
    }

    /**
     * Check if conditions "where" contain item key.
     *
     * @return array|bool the condition value
     */
    protected function conditionsContainKey()
    {
        if (empty($this->where))
        {
            return false;
        }

        $conditionKeys = array_keys($this->where);
        $keys = $this->model->getCompositeKey();

        $conditionsContainKey = count(array_intersect($conditionKeys, $keys)) === count($keys);

        if (!$conditionsContainKey)
        {
            return false;
        }

        $conditionValues = [];

        foreach ($keys as $key)
        {
            $condition = $this->where[$key];
            $conditionValues[$key] = $condition['value'];
        }

        return $conditionValues;
    }

    protected function conditionsContainIndexKey()
    {
        if (empty($this->where))
        {
            return false;
        }

        foreach ($this->model->getIndexes() as $key => $name)
        {
            if (isset($this->where[$key]))
            {
                return $key;
            }
        }

        return false;
    }

    protected function getDynamoDbExpression()
    {
        $expression = '';
        foreach ($this->where as $column => $attrs)
        {
            $expression = '#'.strtolower($column) . $attrs['operator'] . ':v_'.strtolower($column);
        }

        return $expression;
    }

    protected function getDynamoDbAttributeNames()
    {
        $names = [];

        foreach ($this->where as $column => $attrs)
        {
            $key = '#' . strtolower($column);
            $names[$key] = $column;
        }

        return $names;
    }

    protected function getDynamoDbAttributeValues()
    {
        $attributes = [];

        foreach ($this->where as $column => $attrs)
        {
            $attributeItem = [':v_'.strtolower($column) => $attrs['value']];
            $attributes += $this->model->marshalItem($attributeItem);
        }

        return $attributes;
    }
}
