<?php
namespace DynamoDbORM;

use Aws\DynamoDb\DynamodbClient;
use Aws\Dynamodb\Marshaler;

abstract class DynamoDbModel
{
    /**
     * @var \App\Models\DynamoDbClientService
     */
    private static $dynamoDb;

    /**
     * @var \Aws\DynamoDb\DynamoDbClient
     */
    private $dbc;

    /**
     * @var \Aws\DynamoDb\Marshaler
     */
    private $marshaler;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $hashKey;

    /**
     * @var string
     */
    protected $rangeKey;

    /**
     * @var array
     */
    protected $indexes = [];

     /**
      * @var array
      */
    protected $aliases = [];

    public function __construct(DynamoDbClientService $dynamoDbService = null)
    {
        if ($dynamoDbService !== null)
            self::$dynamoDb = $dynamoDbService;

        if (self::$dynamoDb === null)
            throw new \RuntimeException('DynamoDbClientService must be instanciated');

        $this->dbc = self::$dynamoDb->getClient();
        $this->marshaler = self::$dynamoDb->getMarshaler();

        //$this->fill($attributes);
    }

    public function create($attributes = [])
    {
        return new DynamoDbItem($this, $attributes);
    }

    public function getTable()
    {
        return $this->tableName;
    }

    public function getCompositeKey()
    {
        return [$this->hashKey, $this->rangeKey];
    }

    public function hasCompositeKey()
    {
        return $this->rangeKey !== null;
    }

    public function getClient()
    {
        return $this->dbc;
    }

    public function getAliases()
    {
        return $this->aliases;
    }

    public function getIndexes()
    {
        return $this->indexes;
    }

    public function find($id = [], $columns = [])
    {
        $builder = new DynamoDbQueryBuilder($this);

        // If the key is directly pass to the find method,
        // we can return the corresponding item
        if (!empty($id))
        {
            return $builder->find($id)->get($columns);
        }

        return $builder;
    }

    public function findAll($columns = [])
    {
        return $this->find([], $columns)->get();
    }

    public function getLastOperationType()
    {
        return $this->lastOperationType;
    }

    public function marshalItem($item)
    {
        return $this->marshaler->marshalItem($item);
    }

    public function unmarshalItem($item)
    {
        return $this->marshaler->unmarshalItem($item);
    }

    public function __call($method, $parameters)
    {
        $builder = new DynamoDbQueryBuilder($this);
        call_user_func_array([$builder, $method], $parameters);
    }
}
